﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainPrefabLinks : MonoBehaviour 
{
	public GameObject ground;
	public GameObject tileColor;
	public Light spotlight;
}
