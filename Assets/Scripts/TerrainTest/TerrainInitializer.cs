﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainInitializer : MonoBehaviour 
{
	public GameObject terrainPrefab;
	GameObject[,,,] _terrain;
	TerrainPrefabLinks[,,,] _terrainLinks;
	Material[,,,] _terrainMat;
	const int duplicateSize = 2;
	const int arraySize = 10;
	const int arrayHeight = 5;
	
	void Start () 
	{
		_terrain = new GameObject[duplicateSize,arraySize,arraySize,arrayHeight];
		_terrainMat = new Material[duplicateSize,arraySize,arraySize,arrayHeight];
		_terrainLinks = new TerrainPrefabLinks[duplicateSize,arraySize,arraySize,arrayHeight];
		for(int c = 0; c < duplicateSize; c++)
		{
			for(int i = 0; i < arraySize; i++)
			{
				for(int j = 0; j < arraySize; j++)
				{
					for(int k = 0; k < arrayHeight; k++)
					{
						_terrain[c,i,j,k] = Instantiate(terrainPrefab, new Vector3(c * 150 + i * 10, k * 100, j * 10), Quaternion.identity, this.transform);
						_terrainLinks[c,i,j,k] = _terrain[c,i,j,k].GetComponent<TerrainPrefabLinks>();
						_terrainMat[c,i,j,k] = _terrainLinks[c,i,j,k].tileColor.GetComponent<Renderer>().material;
						_terrainMat[c,i,j,k].SetColor("_Color", Color.HSVToRGB((i)/(float)arraySize, (j+1)/(float)arraySize, (k+1)/(float)arrayHeight));
						_terrainMat[c,i,j,k].SetColor("_ColorSplit", Color.HSVToRGB((i)/(float)arraySize, (j+1)/(float)arraySize, (k+1)/(float)arrayHeight));
					}
				}
			}
		}
	}
	
	void Update () 
	{
		if(Input.GetKeyDown(KeyCode.UpArrow))
		{
			for(int c = 0; c < duplicateSize; c++)
			{
				for(int i = 0; i < arraySize; i++)
				{
					for(int j = 0; j < arraySize; j++)
					{
						for(int k = 0; k < arrayHeight; k++)
						{
							_terrainLinks[c,i,j,k].spotlight.intensity += 0.5f;
						}
					}
				}
			}
		}
		if(Input.GetKeyDown(KeyCode.DownArrow))
		{
			for(int c = 0; c < duplicateSize; c++)
			{
				for(int i = 0; i < arraySize; i++)
				{
					for(int j = 0; j < arraySize; j++)
					{
						for(int k = 0; k < arrayHeight; k++)
						{
							_terrainLinks[c,i,j,k].spotlight.intensity -= 0.5f;
						}
					}
				}
			}
		}
	}
}
