﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMeshGenerator : MonoBehaviour 
{
	public GameObject _quad;
	public GameObject _quadGrid;
	Mesh _mesh;	
	Mesh _meshGrid;
	public bool _centered = false;

	int xSize = 10;
	int ySize = 10;
	int yShineSize = 10;
	public Vector3 _vertexOffset = new Vector3(0.0f, 0.0f, 0.0f);
	Vector3 _vOffset = new Vector3(0.0f, 0.0f, 0.0f);
	public Vector3 _vertexScale = new Vector3(1.0f, 1.0f, 1.0f);
	public Vector2 _lensScale = new Vector2(1.0f, 1.0f);

	Vector3[] vertices;

	bool _pauseState = false;
	float _pauseTimer = 0.0f;
	float _pauseSpeed = 2.0f;

	class GridShine
	{
		public float offset;
		public float offsetMax = 5.0f;
		public float speed;
		public void Init()
		{
			SetSpeed();
			offset = Random.Range(0.0f, offsetMax);
		}

		public void ReInit()
		{
			SetSpeed();
			offset = 0.0f;
		}

		void SetSpeed()
		{
			speed = Random.Range(0.85f, 1.75f);
		}

		public void Update()
		{
			offset += speed * Time.deltaTime;
			if(offset > offsetMax)
			{
				ReInit();
			}			
		}
	}

	class MeshData
	{
		public Vector3[] vertices;
		public Vector2[] uv;
		public Color[] colors;
		public int[] triangles;

		public void InitVert(int numOfVerts)
		{
			vertices = new Vector3[numOfVerts];
			uv = new Vector2[numOfVerts];
			colors = new Color[numOfVerts];
		}

		public void InitTris(int numOfTris)
		{
			triangles = new int[numOfTris];
		}

		public void ApplyToMesh(Mesh mesh)
		{
			mesh.vertices = vertices;
			mesh.uv = uv;
			mesh.colors = colors;
			mesh.triangles = triangles;
		}
	}

	MeshData gridMD;
	GridShine[] _gridShine;

	void Awake()
	{	
		_gridShine = new GridShine[xSize];
		for(int i = 0; i < _gridShine.Length; ++i)
		{
			_gridShine[i] = new GridShine();
			_gridShine[i].Init();
		}
		InitializeMesh();
	}
	
	void InitializeMesh()
	{
		_quad.SetActive(true);
		_quadGrid.SetActive(true);
		_mesh = new Mesh();
		_meshGrid = new Mesh();
		_mesh.name = "Pause Effect Mesh";
		_meshGrid.name = "Pause Grid Mesh";
		Generate();
	}

	void Generate()
	{
		if(_centered)
		{
			_vOffset = _vertexOffset - new Vector3(_vertexScale.x * xSize * 0.5f, _vertexScale.y * ySize * 0.5f, 0.0f);
		}

		gridMD = new MeshData();
		gridMD.InitVert((xSize + 1) * (ySize + 1));
		gridMD.InitTris(xSize * ySize * 6);
		vertices = new Vector3[(xSize + 1) * (ySize + 1) + (xSize * 2) * (yShineSize + 1)];		
		Color[] colors = new Color[vertices.Length];
		Vector2[] uv = new Vector2[vertices.Length];
		int i = 0;
		for (int x = 0; x <= xSize; x++) 
		{
			for (int y = ySize; y >= 0; y--, i++) 
			{
				vertices[i] = new Vector3(x * _vertexScale.x, y * _vertexScale.y) + _vOffset;
				uv[i] = new Vector2((float)x / xSize, (float)y / ySize);
				colors[i] = Color.white;
				colors[i].a = _pauseTimer;

				gridMD.vertices[i] = vertices[i];
				gridMD.uv[i] = uv[i];
				gridMD.colors[i] = colors[i];
			}
		}

		// Backup
		//for (int i = 0, y = 0; y <= ySize; y++) 
		//{
		//	for (int x = 0; x <= xSize; x++, i++) 
		//	{
		//		vertices[i] = new Vector3(x * _vertexScale.x, y * _vertexScale.y) + _vOffset;
		//		uv[i] = new Vector2((float)x / xSize, (float)y / ySize);
		//	}
		//}

		int[] triangles = new int[(xSize * ySize * 6) + (xSize * yShineSize * 6)];
		int ti = 0;
		for (int vi = 0, y = 0; y < ySize; y++, vi++) 
		{
			for (int x = 0; x < xSize; x++, ti += 6, vi++) 
			{
				triangles[ti] = vi;
				triangles[ti + 3] = triangles[ti + 2] = vi + 1;
				triangles[ti + 4] = triangles[ti + 1] = vi + xSize + 1;
				triangles[ti + 5] = vi + xSize + 2;

				gridMD.triangles[ti] = triangles[ti];
				gridMD.triangles[ti + 3] = gridMD.triangles[ti + 2] = triangles[ti + 2];
				gridMD.triangles[ti + 4] = gridMD.triangles[ti + 1] = triangles[ti + 1];
				gridMD.triangles[ti + 5] = triangles[ti + 5];
			}
		}

		for (int x = 0, vi = ((xSize + 1) * (ySize + 1)); x < xSize; x++, vi += 2) 
		{
			for (int y = 0; y < yShineSize; ti += 6, y++, vi += 2) 
			{
				triangles[ti] = vi;
				triangles[ti + 3] = triangles[ti + 2] = vi + 1;
				triangles[ti + 4] = triangles[ti + 1] = vi + 2;
				triangles[ti + 5] = vi + 3;
			}
			
		}

		// For the shines
		//verticesShine = new Vector3[(xSize * 2) * (yShineSize + 1)];
		//Color[] colorShine = new Color[(xSize * 2) * (yShineSize + 1)];
		//Vector2[] uvShine = new Vector2[verticesShine.Length];
		
		for (int x = 0; x < xSize; x++) 
		{
			for (int y = yShineSize; y >= 0; y--) 
			{
				for (int c = 0; c < 2; ++c, i++)
				{
					vertices[i] = new Vector3((x + c) * _vertexScale.x, (yShineSize - y / (float)yShineSize) * _vertexScale.y - _gridShine[x].offset + 1) + _vOffset;
					uv[i] = new Vector2((float)(x + c) / xSize, (float)(yShineSize - y / (float)yShineSize) / yShineSize);
					uv[i].y -= _gridShine[x].offset * 0.5f - 0.5f;
					colors[i] = Color.white;
					if(y < yShineSize)
					{
						uv[i].x += Mathf.Pow((float)y / yShineSize, 3.0f) * 0.1f * _pauseTimer;
						colors[i] = Color.white * (Mathf.Pow((float)y / yShineSize, 2.0f) * 4.0f + 1);
					}
					
					colors[i].a = _pauseTimer;
					//colorShine[i] = Color.white * (y * 0.2f + 1);
				}
			}
		}

		LensEffect();
		
		//gridMD.ApplyToMesh(_meshGrid);
		_meshGrid.vertices = gridMD.vertices;
		_meshGrid.uv = gridMD.uv;
		_meshGrid.triangles = gridMD.triangles;
		_meshGrid.colors = gridMD.colors;
		_meshGrid.RecalculateNormals();
		_quadGrid.GetComponent<MeshFilter>().mesh = _meshGrid;

		_mesh.vertices = vertices;
		_mesh.uv = uv;
		_mesh.triangles = triangles;		
		_mesh.colors = colors;
		_mesh.RecalculateNormals();
		_mesh.RecalculateBounds();
		_quad.GetComponent<MeshFilter>().mesh = _mesh;

		//_meshShine.vertices = verticesShine;
		//_meshShine.uv = uvShine;
		//_meshShine.triangles = trianglesShine;
		//_meshShine.colors = colorShine;
		//_meshShine.RecalculateBounds();
		//_meshShine.RecalculateNormals();
		//_quadShine.GetComponent<MeshFilter>().mesh = _meshShine;
	}

	void LensEffect()
	{
		int i = 0;
		for (int y = 0; y <= ySize; y++) 
		{
			for (int x = 0; x <= xSize; x++, i++) 
			{
				vertices[i].x = Mathf.Lerp(vertices[i].x * 0.5f * Screen.width / Screen.height, 1.1f* vertices[i].x / (1.0f + vertices[i].y * vertices[i].y * _lensScale.x), _pauseTimer);
				vertices[i].y = Mathf.Lerp(vertices[i].y * 0.5f, vertices[i].y * (0.5f +  vertices[i].x * vertices[i].x * _lensScale.y), _pauseTimer);

				gridMD.vertices[i] = vertices[i];
			}
		}

		for (int y = 0; y <= yShineSize; y++) 
		{
			for (int x = 0; x < xSize; x++) 
			{
				for (int c = 0; c < 2; ++c, i++)
				{
					vertices[i].x = Mathf.Lerp(vertices[i].x * 0.5f * Screen.width / Screen.height, 1.1f* vertices[i].x / (1.0f + vertices[i].y * vertices[i].y * _lensScale.x), _pauseTimer);
					vertices[i].y = Mathf.Lerp(vertices[i].y * 0.5f, vertices[i].y * (0.5f +  vertices[i].x * vertices[i].x * _lensScale.y), _pauseTimer);
				}
			}
		}
	}

	void Update () 
	{
		if(Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Space))
		{
			_pauseState = !_pauseState;
		}

		if(_pauseState)
		{
			_pauseTimer += Time.deltaTime * _pauseSpeed;
		}
		else
		{
			_pauseTimer -= Time.deltaTime * _pauseSpeed;
		}
		_pauseTimer = Mathf.Clamp01(_pauseTimer);

		for(int i = 0; i < _gridShine.Length; ++i)
			_gridShine[i].Update();
		Generate();
	}
}
