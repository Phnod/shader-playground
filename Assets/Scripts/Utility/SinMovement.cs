﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SinMovement : MonoBehaviour 
{
	public Vector3 _sinSpeed = new Vector3(1.0f, 1.0f, 1.0f);
	public Vector3 _sinAmount = new Vector3(0.01f, 0.0f, 0.0f);
	public Vector3 _cosSpeed = new Vector3(1.0f, 1.0f, 1.0f);
	public Vector3 _cosAmount = new Vector3(0.0f, 0.0f, 0.0f);
	
	void FixedUpdate () 
	{
		float fixedTimePi = Time.fixedTime * Mathf.PI * 2.0f;
		transform.Translate(
			Mathf.Sin(_sinSpeed.x * fixedTimePi) * _sinAmount.x * Time.fixedDeltaTime + Mathf.Cos(_cosSpeed.x * fixedTimePi) * _cosAmount.x * Time.fixedDeltaTime,
			Mathf.Sin(_sinSpeed.y * fixedTimePi) * _sinAmount.y * Time.fixedDeltaTime + Mathf.Cos(_cosSpeed.y * fixedTimePi) * _cosAmount.y * Time.fixedDeltaTime,
			Mathf.Sin(_sinSpeed.z * fixedTimePi) * _sinAmount.z * Time.fixedDeltaTime + Mathf.Cos(_cosSpeed.z * fixedTimePi) * _cosAmount.z * Time.fixedDeltaTime
			);
	}
}
