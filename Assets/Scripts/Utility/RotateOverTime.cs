﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateOverTime : MonoBehaviour 
{
	public enum RotationType
	{
		Constant,
		Curve
	};

	public bool ignoreParentRotations = false;

	public RotationType rotationType = RotationType.Constant;

	delegate void RotateDel();
	RotateDel rotateFunc;
	public Space rotationSpace = Space.Self;

	public Quaternion initialRotation;
	public Vector3 currentRotationVector;
	public Vector3 rotateSpeed = new Vector3(0f, 0f, 0f);
	public AnimationCurve speedCurve;
	public float speedCurveLength = 1f;

	// Use this for initialization
	void Start () 
	{
		if(ignoreParentRotations)
		{
			initialRotation = transform.rotation;
			currentRotationVector = Vector3.zero;
			if(rotationType == RotationType.Constant)
			{
				rotateFunc = RotateConstantNP;
			}
			else
			{
				rotateFunc = RotateCurveNP;
			}
		}
		else
		{
			if(rotationType == RotationType.Constant)
			{
				rotateFunc = RotateConstant;
			}
			else
			{
				rotateFunc = RotateCurve;
			}
		}

		
	}

	
	
	// Update is called once per frame
	void Update () 
	{
		rotateFunc();
	}

	void RotateConstant()
	{
		transform.Rotate(rotateSpeed * Time.deltaTime, rotationSpace);
	}

	void RotateCurve()
	{
		float curveResult = speedCurve.Evaluate(((Time.time / speedCurveLength) % 1));
		transform.Rotate(curveResult * rotateSpeed * Time.deltaTime, rotationSpace);
	}

	void RotateConstantNP()
	{
		currentRotationVector += (rotateSpeed * Time.deltaTime);
		transform.rotation = initialRotation;
		transform.Rotate(currentRotationVector, rotationSpace);
	}

	void RotateCurveNP()
	{
		float curveResult = speedCurve.Evaluate(((Time.time / speedCurveLength) % 1));
		currentRotationVector += (curveResult * rotateSpeed * Time.deltaTime);
		transform.rotation = initialRotation;

		transform.Rotate(currentRotationVector, rotationSpace);
	}
}
