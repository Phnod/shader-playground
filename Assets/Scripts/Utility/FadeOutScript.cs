﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeOutScript : MonoBehaviour 
{
	public AnimationCurve alphaOverTime;
	public float fadeOutTime;
	float updateTime = 0.0f;
	
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		updateTime += Time.deltaTime;
		Color color = this.GetComponent<MeshRenderer>().material.GetColor("_Color");
		color.a = alphaOverTime.Evaluate(updateTime/fadeOutTime);
		this.GetComponent<MeshRenderer>().material.SetColor("_Color", color);
		if(updateTime > fadeOutTime)
		{
			Destroy(this.gameObject);
		}
	}
}
