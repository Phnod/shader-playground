﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FunkyAudioVis : MonoBehaviour 
{
	[System.Serializable]
	public class SpinLayer
	{
		public GameObject _Prefab;
		
		[HideInInspector]
		public GameObject[] _Objects;
		
		[HideInInspector]
		public Material[] _Materials;
		public Material _Material;
		[HideInInspector]
		public GameObject _Parent;
		public float _Offset = 2.5f;
		public float _Depth = 0.0f;
		public float _RotateSpeed = 120.0f;
		public Vector3 _Scale = Vector3.one;
		public bool _ScaleWithSpectrum = true; 
		public Color _Color = Color.white;
		public bool _ColorWithSpectrum = true;
	}
	
	public WaveFormGenerator _AudioGenerator;
	[SerializeField]
	public SpinLayer[] _SpinLayers = new SpinLayer[1];
	Material _BlendBufferMaterial;

	Vector2[] catmullPoints = new Vector2[4];
	float catmullInterp = 0.0f;
	float catmullSpeed = 0.25f;
	
	// Use this for initialization
	void Start () 
	{
		_BlendBufferMaterial = GetComponent<BlendBuffer>().GetMaterial();
		//for(int i = 0; i < _SpinLayers.Length; ++i)		
		//	_SpinLayers[i] = new SpinLayer();

		foreach(SpinLayer Spin in _SpinLayers)
		{
			int arrayLength = _AudioGenerator._SpectrumPower;
			Spin._Objects = new GameObject[arrayLength];
			Spin._Parent = Instantiate(new GameObject(), transform.position, transform.rotation, transform);
			Spin._Materials = new Material[arrayLength];

			for(int i = 0; i < Spin._Objects.Length; ++i)
			{
				float percent = (float)i / (float)Spin._Objects.Length;
				Spin._Objects[i] = Instantiate(Spin._Prefab, Spin._Parent.transform.position, Spin._Parent.transform.rotation, Spin._Parent.transform);
				Spin._Objects[i].transform.Translate(Mathf.Sin(percent * Mathf.PI * 2) * Spin._Offset, Mathf.Cos(percent * Mathf.PI * 2) * Spin._Offset, Spin._Depth);
				Spin._Objects[i].transform.localScale = Spin._Scale;
				Spin._Objects[i].transform.Rotate(0.0f, 0.0f, -percent * 360);
				Spin._Materials[i] = new Material(Spin._Material);
				Spin._Objects[i].GetComponent<Renderer>().material = Spin._Materials[i];
			}
		}

		for(int i = 0; i < catmullPoints.Length; ++i)
		{
			catmullPoints[i] = new Vector2(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f));
		}
	}
	
	Vector2 catmull(Vector2 data0, Vector2 data1, Vector2 data2, Vector2 data3, float t)
	{
		return 0.5f * 
			(t*t*t*(-data0 + 3.0f*data1 - 3.0f*data2 + data3) + 
			t*t*(2.0f*data0 - 5.0f*data1 + 4.0f*data2 - data3) + 
			t* (-data0 + data2) + 
			(2.0f*data1));
	}

	// Update is called once per frame
	void Update ()
	{
		catmullInterp += Time.deltaTime * catmullSpeed;
		if(catmullInterp > 1.0f)
		{
			for(int i = 1; i < catmullPoints.Length; ++i)
			{
				catmullPoints[i-1] = catmullPoints[i];
			}
			catmullPoints[3] = new Vector2(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f));
			catmullInterp -= 1.0f;
		}

		_BlendBufferMaterial.SetVector("_ZoomOrigin", catmull(catmullPoints[0], catmullPoints[1], catmullPoints[2], catmullPoints[3], catmullInterp));

		foreach(SpinLayer Spin in _SpinLayers)
		{
			Spin._Parent.transform.Rotate(0.0f, 0.0f, Time.deltaTime * Spin._RotateSpeed * _AudioGenerator._CurrentAmplitude);
			
			int index = 0;
			foreach(GameObject obj in Spin._Objects)
			{
				if(Spin._ColorWithSpectrum)
				{
					Color newColor = Spin._Color * Color.HSVToRGB(Mathf.Max(0.8f - _AudioGenerator.spectrumSum[index] * 0.8f, 0.0f), 1.0f, 1.0f);
					
					Spin._Materials[index].SetColor("_Color", newColor);
					Spin._Materials[index].SetColor("_EmissionColor", newColor);
				}
				else
				{
					Color newColor = Spin._Color * Color.HSVToRGB(Mathf.Max(0.8f - _AudioGenerator._CurrentAmplitude * 0.8f, 0.0f), 1.0f, 1.0f);
					
					Spin._Materials[index].SetColor("_Color", newColor);
					Spin._Materials[index].SetColor("_EmissionColor", newColor);
				}

				if(Spin._ScaleWithSpectrum)
				{
					obj.transform.localScale = Spin._Scale * (_AudioGenerator.spectrumSum[index] * 0.6f + 0.4f);
				}
				else
				{
					obj.transform.localScale = Spin._Scale * (_AudioGenerator._CurrentAmplitude * 0.6f + 0.4f);
				}
				++index;
			}
		}
	}
}
