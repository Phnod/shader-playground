﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class BlendBuffer : MonoBehaviour 
{
	public Shader _shader;
	public Material _DemoMat;
	[SerializeField]
	RenderTexture renderTexture;
	[SerializeField]
	CustomRenderTexture[] finalTexture;
	Camera _Camera;

	[SerializeField]
	Material _mat;

	CustomRenderTexture activeBuffer;
	CustomRenderTexture inactiveBuffer;

	void Awake() 
	{
		_Camera = GetComponent<Camera>();

		renderTexture = new RenderTexture(Screen.width, Screen.height, 16, RenderTextureFormat.ARGBFloat); // RenderTextureFormat.ARGB32
		renderTexture.Create();
		
		finalTexture = new CustomRenderTexture[2];
		for(int i = 0; i < finalTexture.Length; ++i)
		{
			finalTexture[i] = new CustomRenderTexture(Screen.width, Screen.height);
			finalTexture[i].format = RenderTextureFormat.ARGBFloat;
			finalTexture[i].Create();
		}
		activeBuffer = finalTexture[0];
		inactiveBuffer = finalTexture[1];
		
		_Camera.targetTexture = renderTexture;

		_mat = new Material(_shader);
	}

	public bool STOP = false;
	private void Update()
	{
		_mat.SetTexture("_MainTex", renderTexture);
		_mat.SetTexture("_OldTex", inactiveBuffer);

		if(_DemoMat != null)
		{
			_DemoMat.SetTexture("_MainTex", inactiveBuffer);
		}
		Graphics.Blit(renderTexture, activeBuffer, _mat);
		Graphics.Blit(activeBuffer, inactiveBuffer);
	}
		
	public Material GetMaterial()
	{
		return _mat;
	}
	
}
