﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpectrumVis : MonoBehaviour 
{
	public WaveFormGenerator _Generator;
	public LineRenderer _Line;
	Vector3[] _spectrumPositions;

	// Use this for initialization
	void Start () 
	{
		if(_Generator != null)
		{
			_spectrumPositions = new Vector3[_Generator._SpectrumPower * 2 + 2];
			_Line.positionCount = (_spectrumPositions.Length);
		}
		else
		{
			Debug.LogError("No Line Generator Linked!");
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		_spectrumPositions[0] = Vector3.zero;
		for(int i = 0; i < _Generator._SpectrumPower; ++i)
		{
	 		_spectrumPositions[i*2+1] = new Vector3(i * 10.0f, _Generator.spectrumSum[i] * 50.0f, 0.0f);
			_spectrumPositions[i*2+2] = new Vector3((i+1) * 10.0f, _Generator.spectrumSum[i] * 50.0f, 0.0f);
		}
		_spectrumPositions[_spectrumPositions.Length-1] = new Vector3((_Generator._SpectrumPower)*10.0f, 0.0f, 0.0f);
		_Line.SetPositions(_spectrumPositions);
	}
}
