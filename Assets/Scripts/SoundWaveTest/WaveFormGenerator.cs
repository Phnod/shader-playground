﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveFormGenerator : MonoBehaviour 
{
	public enum ListenType
	{
		AudioListener,
		Clip
	};
	public ListenType _ListenType = ListenType.AudioListener; 
	public AudioClip _Clip;
	float _time;
	public int _LineLength  = 128;
	public int _SpectrumLength = 64;
	
	public int _SpectrumPower;
	[HideInInspector]
	public Vector3[] _LinePositions;
	float[] dataStream;
	float[] dataStreamLeft;
	float[] dataStreamRight;
	
	[HideInInspector]
	public float[] spectrumStream;
	public float[] spectrumSum;
	float[] data;
	int sampleRate;
	public bool _UpdateWaveform = true;
	public bool _UpdateSpectrum = true;
	
	public float _CurrentAmplitude = 0.0f;

	public float height = 5.0f;

	private void Awake()
	{
		if(_ListenType == ListenType.Clip)
		{
			sampleRate = (int)((float)_Clip.samples / _Clip.length);
		}
		_LinePositions = new Vector3[_LineLength];
		data = new float[_LineLength];
		dataStream = new float[_LineLength*4];
		dataStreamLeft = new float[_LineLength*2];
		dataStreamRight = new float[_LineLength*2];
		for(int i = _SpectrumLength; i > 1; i /= 2, ++_SpectrumPower);
		spectrumStream = new float[_SpectrumLength];
		spectrumSum = new float[_SpectrumPower];
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(_ListenType == ListenType.AudioListener)
		{
			if(_UpdateWaveform)
			{	AudioListener.GetOutputData(dataStreamLeft, 0); AudioListener.GetOutputData(dataStreamRight, 1);	}
			if(_UpdateSpectrum)
			{	AudioListener.GetSpectrumData(spectrumStream, 0, FFTWindow.Rectangular);	}
		}
		else
		{
			//_time += Time.deltaTime;
			_Clip.GetData(dataStream, (int)(Time.timeSinceLevelLoad * sampleRate));
			for(int i = 0; i < dataStreamLeft.Length; ++i)
			{
				dataStreamLeft[i] = dataStream[i*2];
				dataStreamRight[i] = dataStream[i*2+1];
			}
		}

		_CurrentAmplitude = 0.0f;			
		for(int i = 0; i < _SpectrumPower; ++i)
		{
			spectrumSum[i] = 0;
			for(int x = 0; x < Mathf.Pow(2.0f, i); ++x)
			{
				spectrumSum[i] += spectrumStream[(int)Mathf.Pow(2.0f, i) - 1 + x];
			}
		}
		
		// Find the peak of the data, and use it to determine centerpoint
		int peak = data.Length / 2;
		float peakVolume = 0.0f;

		for(int i = 0; i < dataStreamLeft.Length; ++i)
		{
			if(Mathf.Abs(dataStreamLeft[i]) > _CurrentAmplitude)
			{
				_CurrentAmplitude = Mathf.Abs(dataStreamLeft[i]);
			}
			if(Mathf.Abs(dataStreamRight[i]) > _CurrentAmplitude)
			{
				_CurrentAmplitude = Mathf.Abs(dataStreamRight[i]);
			}
		}

		for(int i = data.Length / 2; i < dataStreamLeft.Length - data.Length / 2; ++i)
		{
			float distrib = 0;
			distrib = dataStreamLeft[i];
			// If I want to get sort of an average of the peak instead of the absolute peak
			/*
			distrib = dataStream[i] + dataStream[i-16] + dataStream[i+16];
			for(int x = -4; x <= 4; ++x)
			{
				distrib += dataStream[i+x*2];
			}
			*/
			if(distrib > peakVolume)
			{
				peak = i;
				peakVolume = distrib;
			}
		}

		float sum = 0.0f;
		for(int i = 0; i < data.Length; ++i)
		{
			data[i] = dataStreamLeft[i + peak - data.Length / 2];
			sum += data[i];
		}
		for(int i = 0; i < _LinePositions.Length; ++i)
		{
			float sampleSum = data[i];
			_LinePositions[i] = new Vector3(i * 0.1f, sampleSum * height, 0.0f);
		}
		
	}
}
