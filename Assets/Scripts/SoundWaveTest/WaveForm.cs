﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveForm : MonoBehaviour 
{
	public WaveFormGenerator _Generator;
	public LineRenderer _Line;

	// Use this for initialization
	void Start () 
	{
		if(_Generator != null)
		{
			_Line.positionCount = (_Generator._LineLength);
		}
		else
		{
			Debug.LogError("No Line Generator Linked!");
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		_Line.SetPositions(_Generator._LinePositions);
	}
}
