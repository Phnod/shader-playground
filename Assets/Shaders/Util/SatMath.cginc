#if !defined(SAT_MATH_INCLUDED)
#define SAT_MATH_INCLUDED

//////////////////////////
// BASIC MATH FUNCTIONS //
//////////////////////////

float InverseLerp(in float v, in float v0, in float v1) 
{	
	return saturate((v - v0) / (v1 - v0));
}

float2 InverseLerp(in float2 v, in float2 v0, in float2 v1) 
{	
	return saturate((v - v0) / (v1 - v0));
}

float3 InverseLerp(in float3 v, in float3 v0, in float3 v1) 
{	
	return saturate((v - v0) / (v1 - v0));
}

///////////////////////
// COLOR CONVERSIONS //
///////////////////////


static float Epsilon = 1e-10;
 
float3 RGBtoHCV(in float3 RGB)
{
	// Based on work by Sam Hocevar and Emil Persson
	float4 P = (RGB.g < RGB.b) ? float4(RGB.bg, -1.0, 2.0/3.0) : float4(RGB.gb, 0.0, -1.0/3.0);
	float4 Q = (RGB.r < P.x) ? float4(P.xyw, RGB.r) : float4(RGB.r, P.yzx);
	float C = Q.x - min(Q.w, Q.y);
	float H = abs((Q.w - Q.y) / (6 * C + Epsilon) + Q.z);
	return float3(H, C, Q.x);
}

float3 HUEtoRGB(in float H)
{
	float H6 = H * 6;
	float R = abs(H6 - 3) - 1;
	float G = 2 - abs(H6 - 2);
	float B = 2 - abs(H6 - 4);
	return saturate(float3(R,G,B));
}

// HSV/RGB Conversions
// Hue Saturation Value
float3 RGBtoHSV(in float3 RGB)
{
	float3 HCV = RGBtoHCV(RGB);
	float S = HCV.y / (HCV.z + Epsilon);
	return float3(HCV.x, S, HCV.z);
}

float3 HSVtoRGB(in float3 HSV)
{
	float3 RGB = HUEtoRGB(frac(HSV.x));
	return ((RGB - 1) * HSV.y + 1) * HSV.z;
}

float3 HSVtoRGBclamped(in float3 HSV)
{
	float3 RGB = HUEtoRGB(HSV.x);
	return ((RGB - 1) * HSV.y + 1) * HSV.z;
}

// HSL/RGB Conversions
// Hue Saturation Luminance
float3 HSLtoRGB(in float3 HSL)
{
	float3 RGB = HUEtoRGB(HSL.x);
	float C = (1 - abs(2 * HSL.z - 1)) * HSL.y;
	return (RGB - 0.5) * C + HSL.z;
}

float3 RGBtoHSL(in float3 RGB)
{
  float3 HCV = RGBtoHCV(RGB);
  float L = HCV.z - HCV.y * 0.5;
  float S = HCV.y / (1 - abs(L * 2 - 1) + Epsilon);
  return float3(HCV.x, S, L);
}

///////////////////////
// SLOW ALERT        //
///////////////////////

//float3 RGB2HSV(in float3 c) 
//{
//	float4 K = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
//	float4 p = c.g < c.b ? float4(c.bg, K.wz) : float4(c.gb, K.xy);
//    float4 q = c.r < p.x ? float4(p.xyw, c.r) : float4(c.r, p.yzx);
//	float d = q.x - min(q.w, q.y);
//	return float3(abs(q.z + (q.w - q.y) / (6.0 * d + Epsilon)), d / (q.x + Epsilon), q.x);
//}
//
//float3 rgb2hsv(in float3 c)
//{
//    float4 K = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
//    float4 p = lerp(float4(c.bg, K.wz), float4(c.gb, K.xy), step(c.b, c.g));
//    float4 q = lerp(float4(p.xyw, c.r), float4(c.r, p.yzx), step(p.x, c.r));
//    float d = q.x - min(q.w, q.y);
//    float e = 1.0e-10;
//    return float3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
//}
//
//float3 HSV2RGB (in float3 c) 
//{	
//    float4 K = float4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
//    float3 p = abs(frac(c.xxx + K.xyz) * 6.0 - K.www);
//    return c.z * lerp(K.xxx, saturate(p - K.xxx, 0.0, 1.0), c.y);
//}
//
//float3 hsv2rgb(in float3 c)
//{
//	c = float3(c.x, clamp(c.yz, 0.0, 1.0));
//	float4 K = float4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
//	float3 p = abs(frac(c.xxx + K.xyz) * 6.0 - K.www);
//	return c.z * lerp(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
//}

///////////////////////
// END OF DOCUMENT   //
///////////////////////

#endif