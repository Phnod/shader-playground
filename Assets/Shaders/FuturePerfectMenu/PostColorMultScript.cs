﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PostColorMultScript : MonoBehaviour 
{
	public Shader _shader;
	public Color _color = Color.white;
	Material _material;
	// Use this for initialization
	void Start () 
	{
		_material = new Material(_shader);
	}
	
	private void OnRenderImage(RenderTexture src, RenderTexture dest)
	{
		_material.SetColor("_Color", _color);
		Graphics.Blit(src, dest, _material);
	}
}
