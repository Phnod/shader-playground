﻿Shader "Stephen/Unlit/GrabPassPause"
{
	Properties
	{
		_Color ("Color", Color) = (1,1,1,1)
	}
	SubShader
	{
		// Draw ourselves after all opaque geometry
        Tags { "Queue" = "Transparent+1" }
		ZWrite Off
        // Grab the screen behind the object into _GrabColorMultTexture
        GrabPass
        {
            "_GrabColorPauseTexture"
        }

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float4 color : COLOR;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 color : COLOR;
				//float4 grabPos : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			//sampler2D _MainTex;
			//float4 _MainTex_ST;
			sampler2D _GrabColorPauseTexture;
			float4 _Color;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				//o.vertex = v.vertex;
				//o.vertex.xy *= 2;
				o.color = v.color;
				o.uv = v.uv;
				//o.grabPos = ComputeGrabScreenPos(o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				//fixed4 col = _Color * tex2Dproj(_GrabColorPauseTexture, i.grabPos);
				fixed4 grabColor = tex2D(_GrabColorPauseTexture, i.uv);
				fixed4 col = lerp(grabColor, i.color * _Color * grabColor, _Color.a * i.color.a);
				return col;
			}
			ENDCG
		}
	}
}
