﻿Shader "Stephen/Unlit/GrabPassColorMult"
{
	Properties
	{
		_Color ("Color", Color) = (1,1,1,1)
	}
	SubShader
	{
		// Draw ourselves after all opaque geometry
        Tags { "Queue" = "Transparent+1" }
		ZWrite Off
        // Grab the screen behind the object into _GrabColorMultTexture
        GrabPass
        {
            "_GrabColorMultTexture"
        }

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				//float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				//float2 uv : TEXCOORD0;
				float4 grabPos : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			//sampler2D _MainTex;
			//float4 _MainTex_ST;
			sampler2D _GrabColorMultTexture;
			float4 _Color;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				//o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				//o.grabPos = o.vertex;
				o.grabPos = ComputeGrabScreenPos(o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = _Color * tex2Dproj(_GrabColorMultTexture, i.grabPos);
				return col;
			}
			ENDCG
		}
	}
}
