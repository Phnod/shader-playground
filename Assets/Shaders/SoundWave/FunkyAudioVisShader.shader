﻿Shader "Hidden/FunkyAudioVisShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_OldTex ("Old Tex", 2D) = "white" {}
		_OldAmount("Old Amount", Range(0,1)) = 0.95
		_ZoomSpeed("Zoom Speed", Float) = 0.99
		_ZoomOrigin("Zoom Origin", Vector) = (0.5, 0.5, 0.0, 0.0)
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			float rand(float2 co)
			{
				return frac(sin(1000.0*dot(co.xy, float2(31.7395, 27.142))) * 327584.3);
			}

			float rand(float co)
			{
				return frac(sin(1000.0 * co) * 327584.3);
			}

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			sampler2D _OldTex;
			sampler2D _SelfTexture2D;

			float _OldAmount;
			float _ZoomSpeed;
			float4 _ZoomOrigin;

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);
				fixed4 old = tex2D(_OldTex, (i.uv - _ZoomOrigin.xy) * _ZoomSpeed + _ZoomOrigin.xy);
				return fixed4(lerp(lerp(col.rgb, old.rgb, _OldAmount), rand(i.uv + _Time.r), 0.00), 1.0);
				//return lerp(col, old, 0.0);
			}
			ENDCG
		}
	}
}
