﻿Shader "Unlit/SoundWaveFakeShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_OffsetRange ("Ranges", Vector) = (1,1,1,1)
		_OffsetFreq ("Freq", Vector) = (10,10,10,10)
		_OffsetSpeed ("Speed", Vector) = (1,1,1,1)
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" "Queue"="Transparent"}
		LOD 100
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}

			float4 _OffsetRange;
			float4 _OffsetSpeed;
			float4 _OffsetFreq;
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				float offset = 
				sin((2.1 + i.uv) * _OffsetFreq.x + _OffsetSpeed.x * _Time.g * 10) * _OffsetRange.x + 
				sin((6.1 + i.uv) * _OffsetFreq.y + _OffsetSpeed.y * _Time.g * 10) * _OffsetRange.y + 
				sin((3.1 + i.uv) * _OffsetFreq.z + _OffsetSpeed.z * _Time.g * 10) * _OffsetRange.z + 
				sin((1.1 + i.uv) * _OffsetFreq.w + _OffsetSpeed.w * _Time.g * 10) * _OffsetRange.w;

				offset /= (_OffsetRange.x + _OffsetRange.y + _OffsetRange.z + _OffsetRange.w) * 3.0;

				
				fixed4 col = tex2D(_MainTex, i.uv + float2(0, offset));
				// apply fog
				UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}
			ENDCG
		}
	}
}
