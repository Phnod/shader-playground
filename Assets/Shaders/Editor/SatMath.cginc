#if !defined(SAT_MATH)
#define SAT_MATH

float InverseLerp(float v, float v0, float v1) 
{	
	return saturate((v - v0) / (v1 - v0));
}

float2 InverseLerp(float2 v, float2 v0, float2 v1) 
{	
	return saturate((v - v0) / (v1 - v0));
}

float3 InverseLerp(float3 v, float3 v0, float3 v1) 
{	
	return saturate((v - v0) / (v1 - v0));
}

float rand(float2 co)
{
	return frac(sin(1000.0*dot(co.xy, float2(21.5739, 43.421))) * 617284.3);
}

float rand(float co)
{
	return frac(sin(1000.0 * co) * 617284.3);
}

#endif