﻿using UnityEngine;
using UnityEngine.Rendering;
using UnityEditor;
using System;

public class UIShaderGUI : ShaderGUI 
{
	enum RenderingMode 
    {
		Opaque, Cutout, Fade
	}

	enum CullingMode 
    {
		Off = 0, Front = 1, Back = 2
	}

	struct RenderingSettings 
    {
		public RenderQueue queue;
		public string renderType;
		public BlendMode srcBlend, dstBlend;
		public bool zWrite;

		public static RenderingSettings[] modes = 
        {
			new RenderingSettings() 
            {
				queue = RenderQueue.Geometry,
				renderType = "",
				srcBlend = BlendMode.One,
				dstBlend = BlendMode.Zero,
				zWrite = true
			},
			new RenderingSettings() 
            {
				queue = RenderQueue.Geometry,
				//queue = RenderQueue.AlphaTest,
				renderType = "TransparentCutout",
				srcBlend = BlendMode.One,
				dstBlend = BlendMode.Zero,
				zWrite = true
			},
			new RenderingSettings() 
            {
				queue = RenderQueue.Transparent,
				renderType = "Transparent",
				srcBlend = BlendMode.SrcAlpha,
				dstBlend = BlendMode.OneMinusSrcAlpha,
				zWrite = false
			},
		};
	}

	struct CullSettings 
    {
		public CullMode cull;
		public static CullSettings[] modes = 
        {
			new CullSettings() 
            {
				cull = CullMode.Off
			},
			new CullSettings() 
            {
				cull =  CullMode.Front
			},
			new CullSettings() 
            {
				cull =  CullMode.Back
			}
		};
	}

	
	static GUIContent staticLabel = new GUIContent();

	Material target;
	MaterialEditor editor;
	MaterialProperty[] properties;
	bool shouldShowAlphaCutoff;

	public override void OnGUI (MaterialEditor editor, MaterialProperty[] properties) 
    {
		this.target = editor.target as Material;
		this.editor = editor;
		this.properties = properties;
		DoRenderingMode();
		DoCullingMode();
		DoOffset();
		DoMain();
		DoRenderQueue();
	}


	void DoRenderingMode () 
    {
		RenderingMode mode = RenderingMode.Opaque;
		shouldShowAlphaCutoff = false;
		if (IsKeywordEnabled("_RENDERING_CUTOUT")) 
        {
			mode = RenderingMode.Cutout;
			shouldShowAlphaCutoff = true;
		}
		else if (IsKeywordEnabled("_RENDERING_FADE")) 
        {
			mode = RenderingMode.Fade;
		}

		EditorGUI.BeginChangeCheck();
		mode = (RenderingMode)EditorGUILayout.EnumPopup(
			MakeLabel("Rendering Mode"), mode);
		
		

		if (EditorGUI.EndChangeCheck()) 
        {
			RecordAction("Rendering Mode");
			SetKeyword("_RENDERING_CUTOUT", mode == RenderingMode.Cutout);
			SetKeyword("_RENDERING_FADE", mode == RenderingMode.Fade);
			
			RenderingSettings settings = RenderingSettings.modes[(int)mode];
			FindProperty("_RenderQueue").floatValue = (float)settings.queue;

			foreach (Material m in editor.targets) 
            {
				m.renderQueue = (int)settings.queue;
				m.SetOverrideTag("RenderType", settings.renderType);
				m.SetInt("_SrcBlend", (int)settings.srcBlend);
				m.SetInt("_DstBlend", (int)settings.dstBlend);
				m.SetInt("_ZWrite", settings.zWrite ? 1 : 0);
			}
		}

		
		
	}

	void DoOffset()
	{
		MaterialProperty offsetAmount = FindProperty("_OffsetAmount");
		MaterialProperty offsetAngle = FindProperty("_OffsetAngle");
		Vector2 offset = new Vector2
		(
			offsetAngle.floatValue,
			offsetAmount.floatValue
		);

		Vector2 newOffset = EditorGUILayout.Vector2Field(MakeLabel("Z-Offset", "Y is the amount of Offset, with X influencing based on surface angle"), offset);
		
		if(newOffset != offset)
		{
			Debug.Log("New Offset");
			foreach (Material m in editor.targets) 
            {
				offsetAngle.floatValue = newOffset.x;
				offsetAmount.floatValue = newOffset.y;
			}
		}
	}
	
	void DoRenderQueue()
	{
		//int renderQueue = RenderingSettings.modes[(int)mode].queue;
		//int renderQueue = 
		MaterialProperty renderQueueProperty = FindProperty("_RenderQueue");
		int renderQueue = (int)renderQueueProperty.floatValue;

		RenderQueue queueEnum = (RenderQueue)renderQueue;

		//int newRenderQueue = EditorGUILayout.IntField(MakeLabel("Z-Offset", "Y is the amount of Offset, with X influencing based on surface angle"), offset);
		int newRenderQueue = Convert.ToInt32(EditorGUILayout.EnumPopup(MakeLabel("Render Queue"), queueEnum));
		newRenderQueue = EditorGUILayout.IntField(MakeLabel("Render Queue"), newRenderQueue);
		
		renderQueueProperty.floatValue = newRenderQueue;
		if(newRenderQueue != renderQueue)
		{
			Debug.Log("New RenderQueue");
			foreach (Material m in editor.targets) 
            {
				m.renderQueue = (int)newRenderQueue;
			}
		}
	}


	void DoCullingMode()
	{
		MaterialProperty type = FindProperty("_Cull");
		CullingMode mode = (CullingMode)type.floatValue;

		EditorGUI.BeginChangeCheck();
		mode = (CullingMode)EditorGUILayout.EnumPopup(
			MakeLabel("Culling Mode"), mode);
		if (EditorGUI.EndChangeCheck()) 
        {
			RecordAction("Culling Mode");

			CullSettings settings = CullSettings.modes[(int)mode];
			foreach (Material m in editor.targets) 
            {
				m.SetInt("_Cull", (int)settings.cull);
			}
		}
	}

	void DoMain () 
    {
		GUILayout.Label("Main Maps", EditorStyles.boldLabel);

		MaterialProperty mainTex = FindProperty("_MainTex");
		editor.TexturePropertySingleLine(
			MakeLabel(mainTex, "Base (RGB), Alpha (A)"), mainTex, FindProperty("_Color"));

		if (shouldShowAlphaCutoff) 
        {
			DoAlphaCutoff();
		}
		editor.TextureScaleOffsetProperty(mainTex);
	}

	void DoAlphaCutoff () 
    {
		MaterialProperty slider = FindProperty("_AlphaCutoff");
		EditorGUI.indentLevel += 2;
		editor.ShaderProperty(slider, MakeLabel(slider));
		EditorGUI.indentLevel -= 2;
	}

	MaterialProperty FindProperty (string name) 
    {
		return FindProperty(name, properties);
	}

	static GUIContent MakeLabel (string text, string tooltip = null) 
    {
		staticLabel.text = text;
		staticLabel.tooltip = tooltip;
		return staticLabel;
	}

	static GUIContent MakeLabel (
		MaterialProperty property, string tooltip = null) 
    {
		staticLabel.text = property.displayName;
		staticLabel.tooltip = tooltip;
		return staticLabel;
	}

	void SetKeyword (string keyword, bool state) 
    {
		if (state) {
			foreach (Material m in editor.targets) 
            {
				m.EnableKeyword(keyword);
			}
		}
		else {
			foreach (Material m in editor.targets) 
            {
				m.DisableKeyword(keyword);
			}
		}
	}

	bool IsKeywordEnabled (string keyword) 
    {
		return target.IsKeywordEnabled(keyword);
	}

	void RecordAction (string label) 
    {
		editor.RegisterPropertyChangeUndo(label);
	}
}
