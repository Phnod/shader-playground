﻿Shader "Unlit/RadarShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_CloudTex ("Cloud Texture", 2D) = "black" {}
		_GradientTex ("Gradient Ramp", 2D) = "black" {}
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" "Queue"="Transparent"}
		LOD 100
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float2 uvCloud : TEXCOORD1;
				UNITY_FOG_COORDS(2)
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			sampler2D _CloudTex;
			float4 _CloudTex_ST;
			sampler2D _GradientTex;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.uvCloud = TRANSFORM_TEX(v.uv, _CloudTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}

			float2 RotateAroundXInDegrees (float2 vertex, float degrees)
     		{
     		    float alpha = degrees * UNITY_PI / 180.0;
     		    float sina, cosa;
     		    sincos(alpha, sina, cosa);
     		    float2x2 m = float2x2(cosa, -sina, sina, cosa);
     			return float2(mul(m, vertex.xy)).xy;
     		}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv);
				float cloud = tex2D(_CloudTex, i.uvCloud + _Time.r);
				col.a *= cloud;
				//float radius = tex2D(_GradientTex, i.uv);
				//col *= frac(radius - _Time.g);
				float radius = tex2D(_GradientTex, 0.5 + RotateAroundXInDegrees(i.uv - 0.5, _Time.g * 100));
				//radius = saturate(radius * 2 - 1);
				//col.a *= pow(radius, 4.0);
				col.a *= pow(radius, 6.0);
				// apply fog
				UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}

			
			ENDCG
		}
	}
}
