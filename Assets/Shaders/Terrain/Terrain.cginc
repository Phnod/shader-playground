﻿#if !defined(SAT_TERRAIN_INCLUDED)
#define SAT_TERRAIN_INCLUDED

#include "UnityCG.cginc"
struct appdata
{
	float4 vertex : POSITION;
	float2 uv : TEXCOORD0;
	UNITY_VERTEX_INPUT_INSTANCE_ID
};

struct v2f
{
	float2 uvMain : TEXCOORD0;
	UNITY_FOG_COORDS(4)	
	float4 vertex : SV_POSITION;
	float3 worldPos : TEXCOORD1;
	float4 grabPos : TEXCOORD2;

	#if defined(_SHINY_RAMP) || defined(_DITHER_MAP)
		float4 screenPos : TEXCOORD3;
	#endif
	UNITY_VERTEX_INPUT_INSTANCE_ID
};

sampler2D _MainTex;
float4 _MainTex_ST;
sampler2D _SplitTex;

UNITY_INSTANCING_BUFFER_START(Props)
UNITY_DEFINE_INSTANCED_PROP(float4, _Color)
UNITY_DEFINE_INSTANCED_PROP(float4, _ColorSplit)
UNITY_DEFINE_INSTANCED_PROP(float, _CloudCutoff)
UNITY_INSTANCING_BUFFER_END(Props)

sampler2D _CloudTex;
float4 _CloudColor;
float4 _CloudSplitScale;

float _Intensity;
float _IntensitySplit;
float _DarkVision;

sampler2D _ShinyRamp;
float4 _ShinyColor;
float _ShinySpeed;
float4 _ShinyPosSize;

sampler2D _TerrainGrabPass;

v2f vertTerrain (appdata v)
{
	v2f o;

	UNITY_SETUP_INSTANCE_ID(v);
   	UNITY_TRANSFER_INSTANCE_ID(v, o);

	o.vertex = UnityObjectToClipPos(v.vertex);
	//#if defined(_SHINY_RAMP_WORLD_POS)
		o.worldPos = mul(unity_ObjectToWorld, v.vertex);
	//#endif
	o.uvMain = TRANSFORM_TEX(v.uv, _MainTex);
	    
	o.grabPos = ComputeGrabScreenPos(o.vertex);
	#if defined(_SHINY_RAMP) || defined(_DITHER_MAP)
  		o.screenPos = ComputeScreenPos(o.vertex);
	#endif
	UNITY_TRANSFER_FOG(o,o.vertex);
	return o;
}

float4 fragTerrain (v2f i) : SV_Target
{
	// Get the color of the tile
	float4 Color = UNITY_ACCESS_INSTANCED_PROP(Props, _Color);

	// If Split is active, get the split color
	#if defined(_COLOR_SPLIT) || defined(_CLOUD_COLOR_SPLIT)
		float4 stencilSplit = tex2D(_SplitTex, i.worldPos.xz * _CloudSplitScale.zw);
		#if defined(_COLOR_SPLIT_SHARE)
			// Sample from Split Texture
			// Lerp between splits based on the split texture
			Color = lerp(Color, UNITY_ACCESS_INSTANCED_PROP(Props, _ColorSplit), stencilSplit);
		#endif
	#endif

	// _MainTex is the texture taken for the tile shape
	// Alpha will remove the effect, with black to white options so that non-flat shading and borders can be achieved 
	float4 tileCol = tex2D(_MainTex, i.uvMain);
	float4 col = tileCol; 
			
	float CloudCutoff = UNITY_ACCESS_INSTANCED_PROP(Props, _CloudCutoff);
	// Wacky coordinate stuff
	float cutoff = tex2D(_CloudTex, i.worldPos.xz * _CloudSplitScale.xy + _Time.r * 5.0).r;
	// Add another version of itself, and then divide by 2 (since we did it twice)
	cutoff = (cutoff + tex2D(_CloudTex, i.worldPos.xz * _CloudSplitScale.xy * 0.6 - _Time.r * 4.0).r) / 2.0;
	// Lerp between original color and threshold
	
	if(CloudCutoff - cutoff > 0)
	{
		// This first block only activates when the threshold has been passed
		// and the clouds don't have a special color
	#if defined(_CLOUD_COLOR)
		#if defined(_CLOUD_COLOR_SPLIT) && defined(_COLOR_SPLIT)
			col = lerp(Color, _CloudColor * _IntensitySplit, stencilSplit);			
		#else
			col *= _CloudColor;
		#endif
	#else
		#if defined(_CLOUD_COLOR_SPLIT) && defined(_COLOR_SPLIT)
			col *= lerp(Color, 0, stencilSplit);	
		#else	
			col.a *= 0;
		#endif
	#endif
	}
	else
	{
		col *= Color;
	}

	//col.a = saturate(col.a * (cutoff - (1.0 - CloudCutoff)));
	
	#if defined(_SHINY_RAMP)
		#if !defined(_SHINY_RAMP_WORLD_POS)
			float2 screenUV = i.screenPos.xy / i.screenPos.w;
			float offset = tex2D(_ShinyRamp, float2(screenUV.x, screenUV.y)).w;
				screenUV = (-_Time.r * _ShinySpeed + offset * 0.04 + (screenUV.y + screenUV.x * 0.75)*0.3).xx;
			#if defined(_SHINY_RAMP_OFFSET)
				screenUV += Color.r * 4.4 + Color.g + Color.b * 5.1;
			#endif
			#if defined(_SHINY_RAMP_COLOR_MULT)
				col.rgb += tileCol * _ShinyColor * tex2D(_ShinyRamp, screenUV).rgb * Color;
			#else
				col.rgb += tileCol * _ShinyColor * tex2D(_ShinyRamp, screenUV).rgb;
			#endif
		#else
			float offset = tex2D(_ShinyRamp, i.worldPos.xz).w;
				i.worldPos.xz = (i.worldPos.xz * _ShinyPosSize.xz) + (-_Time.rr * _ShinySpeed + offset * 0.04);
			#if defined(_SHINY_RAMP_OFFSET)
				i.worldPos.xz += Color.r * 4.4 + Color.g + Color.b * 5.1;
			#endif
			#if defined(_SHINY_RAMP_COLOR_MULT)
				col.rgb += tileCol * _ShinyColor * tex2D(_ShinyRamp, float2(i.worldPos.x + i.worldPos.z, i.worldPos.y)).rgb * Color;
			#else
				col.rgb += tileCol * _ShinyColor * tex2D(_ShinyRamp, float2(i.worldPos.x + i.worldPos.z, i.worldPos.y)).rgb;
			#endif
		#endif
	#endif

	

	// This activates if the color of the tile texture is darker than white
	//if(col.a < 0.9 && tileCol.r < 0.9)
	//{
	//	col.a = 1-tileCol.r;
	//}
	//col.a *= tileCol.r;
	
	col.a *= _Intensity;
	#if defined(_COLOR_SPLIT)
		col.a *= lerp(1.0, _IntensitySplit, stencilSplit);
	#endif

	col.a = lerp(1, col.a, pow(tileCol.r, 0.25));
	col.a = lerp(0, col.a, tileCol.a);

	col.rgb *= tex2Dproj(_TerrainGrabPass, i.grabPos).rgb * 2.0 + _DarkVision;


	UNITY_APPLY_FOG(i.fogCoord, col);
	return col;
}


struct v2fOutline
{
	float2 uvMain : TEXCOORD0;
	UNITY_FOG_COORDS(1)	
	float4 vertex : SV_POSITION;
};

v2fOutline vertTerrainOutline (appdata v)
{
	v2fOutline o;

	o.vertex = UnityObjectToClipPos(v.vertex);
	o.uvMain = TRANSFORM_TEX(v.uv, _MainTex);
	UNITY_TRANSFER_FOG(o,o.vertex);
	return o;
}

float4 fragTerrainOutline (v2fOutline i) : SV_Target
{
	float4 col = tex2D(_MainTex, i.uvMain);
	col.a *= 1 - (col.r + col.g + col.b) * 0.333333333;
	col.rgb = 0;
	return col;
}

#endif