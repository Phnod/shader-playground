﻿Shader "Stephen/Terrain/ColorShadingRing"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_ColorMult ("Texture Color", Color) = (0.5, 0.5, 0.5, 0.5)

		_AlphaCutoff("Alpha Cutoff", Range(0,1)) = 0.5

		_CutoffTex("Cutoff Texture", 2D) = "grey" {}
		_Cutoff("Cutoff", Range(0,1)) = 1 // Cutoff is 0-1 for cooldown, with 1 being ready
		_CutoffColor("Cutoff Color", Color) = (0.5,0.5,0.5,1.0)

		//_ShinyRamp("Shiny Ramp (RGB)", 2D) = "black" {}
		//[HDR]_ShinyColor("Shiny Color", Color) = (1,1,1,1)
		//_ShinySpeed("Shiny Speed", Float) = 8.0
		//_ShinyPosSize("Shiny Scale", Float) = 1.0


		[HideInInspector] _OffsetAmount("Offset Amount", Float) = -40
		[HideInInspector] _OffsetAngle("Offset Angle", Float) = -150000
	}
	Category
	{
		Tags { "Queue"="Transparent-10" "RenderType"="Opaque"}
		//Tags { "Queue"="Transparent" "RenderType"="Opaque"}
		// "DisableBatching" = "true"

		SubShader
		{
			LOD 100
			ZWrite Off
			Blend SrcAlpha OneMinusSrcAlpha
			Offset [_OffsetAngle], [_OffsetAmount]


			Pass
			{
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment fragTerrainRing

				
				#include "UnityCG.cginc"
				// make fog work
				#pragma multi_compile_fog
				
				struct appdata
				{
					float4 vertex : POSITION;
					float2 uv : TEXCOORD0;
				};
	
				struct v2f
				{
					float2 uv : TEXCOORD0;
					UNITY_FOG_COORDS(1)
					float4 vertex : SV_POSITION;
				};
	
				sampler2D _MainTex;
				sampler2D _CutoffTex; 
				float _Cutoff;
				float4 _Color;
				float4 _CutoffColor;
				float4 _MainTex_ST;
				float _AlphaCutoff;
				
				v2f vert (appdata v)
				{
					v2f o;
					o.vertex = UnityObjectToClipPos(v.vertex);
					o.uv = TRANSFORM_TEX(v.uv, _MainTex);
					UNITY_TRANSFER_FOG(o,o.vertex);
					return o;
				}

				UNITY_INSTANCING_BUFFER_START(Props)
				UNITY_DEFINE_INSTANCED_PROP(float4, _ColorMult)
				UNITY_INSTANCING_BUFFER_END(Props)
				
				float4 fragTerrainRing(v2f i) : SV_Target
				{
					float4 col = tex2D(_MainTex, i.uv) * UNITY_ACCESS_INSTANCED_PROP(Props, _ColorMult);
					clip(col.a - _AlphaCutoff);
					// Change this when the UVs are rotated
					float2 clipUVs = float2(i.uv.y, 1-i.uv.x);
					float clipper = tex2D(_CutoffTex, clipUVs).r;
					if (clipper - _Cutoff * 1.01 < 0.0f)
					{
					
					}
					else
					{
						discard;
						col.rgb = lerp(col.rgb , _CutoffColor.rgb, _CutoffColor.a);
					}
					//clip(clipper - _Cutoff * 1.01);

					// apply fog
					UNITY_APPLY_FOG(i.fogCoord, col);
					return col;
				}

				ENDCG
			}
		}
	}
}
