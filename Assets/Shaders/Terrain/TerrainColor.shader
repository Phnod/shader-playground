﻿Shader "Stephen/Terrain/ColorShading"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Color ("Texture Color", Color) = (0.5, 0.5, 0.5, 0.5)
		_SplitTex("Split Texture", 2D) = "white" {}
		_ColorSplit ("Texture Color Split", Color) = (0.5, 0.5, 0.5, 0.5)

		_Intensity ("Intensity" , Range(0,1)) = 1
		_IntensitySplit ("Split Intensity" , Range(0,1)) = 1

		_DarkVision ("Dark Vision", Range(0,0.5)) = 0.05

		_ShinyRamp("Shiny Ramp (RGB)", 2D) = "black" {}
		[HDR]_ShinyColor("Shiny Color", Color) = (1,1,1,1)
		_ShinySpeed("Shiny Speed", Float) = 8.0
		_ShinyPosSize("Shiny Scale", Vector) = (0.001, 0.001, 0.001, 0.001)

		_CloudTex ("Cloud Texture", 2D) = "grey" {}
		_CloudCutoff("Cloud Threshold", Range(0,1)) = 1.0
		_CloudColor("Cloud Color", Color) = (1,1,1,1)
		_CloudSplitScale("Cloud & Split Scale", Vector) = (0.05, 0.05, 1.5, 1.5)


		[HideInInspector] _OffsetAmount("Offset Amount", Float) = 0
		[HideInInspector] _OffsetAngle("Offset Angle", Float) = 0

		[HideInInspector] _SrcBlend ("_SrcBlend", Int) = 2
		[HideInInspector] _DstBlend ("_DstBlend", Int) = 0

		[HideInInspector] _BlendSetting("Blend Setting", Int) = 0
	}
	Category
	{
		Tags { "Queue"="Geometry-5" "RenderType"="Opaque" "IgnoreProjector"="True"}
		//Tags { "Queue"="Transparent" "RenderType"="Opaque"}
		// "DisableBatching" = "true"

		SubShader
		{
			//Blend DstColor Zero
			Blend [_SrcBlend] [_DstBlend]
			ZWrite Off
			//LOD 100
			Offset [_OffsetAngle], [_OffsetAmount]

			GrabPass
        	{
        	    "_TerrainGrabPass"
        	}

			Pass
			{
				CGPROGRAM
				#pragma vertex vertTerrain
				#pragma fragment fragTerrain

				#pragma shader_feature _SHINY_RAMP
				#pragma shader_feature _SHINY_RAMP_WORLD_POS
				#pragma shader_feature _SHINY_RAMP_OFFSET
				#pragma shader_feature _SHINY_RAMP_COLOR_MULT
				#pragma shader_feature _COLOR_SPLIT
				#pragma shader_feature _COLOR_SPLIT_SHARE
				#pragma shader_feature _CLOUD_COLOR
				#pragma shader_feature _CLOUD_COLOR_SPLIT

				// make fog work
				#pragma multi_compile_fog
				
				#include "Terrain.cginc"

				ENDCG
			}

			Pass
			{
				CGPROGRAM
				#pragma vertex vertTerrainOutline
				#pragma fragment fragTerrainOutline
				
				// make fog work
				#pragma multi_compile_fog

				#include "Terrain.cginc"

				ENDCG
			}
		}
	}
	CustomEditor "TerrainShaderGUI"
}
